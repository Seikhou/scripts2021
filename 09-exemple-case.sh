#! /bin/bash
# @Seikhou ASIX M01-ISO
# 
# Exemples case
# -------------------

case $1 in
	[aeiou])
		echo "és una vocal"
	*)
		ech "és una consonant";;
esac
exit #! /bin/bash
# @Seikhou ASIX M01-ISO
#
# Exemples case
# -------------------

# dl dt dc dj dv ds dm
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
    echo "$1 és un dia laborable";;
  "ds"|"dm")
    echo "$1 és festiu";;
  *)
    echo "això ($1) no és un dia";;
esac
exit 0


case $1 in
  [aeiou])
    echo "$1 és una vocal";;
  [bcdfghjklmnpqrstvwxyz])
    echo "$1 és una consonant";;
  *)
    echo "una altra cosa";;
esac
exit 0


case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "és una nena"
    ;;
  *)
     echo "indefinit";;
esac
exit 0


