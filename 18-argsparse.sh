#! /bin/bash
# @Seikhou ASIX-M01
# Març
# Programa que procesa opcions amb valor i arguments
# -------------------------------

opcions=""
args=""
file=""
nums=""

while [ -n "$1" ]
do
	case $1 in
		-[bce])
			opcions="$opcions $1";;
		-a)
			file=$2
			shift;;
		-d)
			nums=$2
			shift;;
		*)
			args="$args $1";;

	esac
	shift	
done
echo "Opcions: $opcions"
echo "Arguments: $args"
echo "File: $file"
echo "Nums: $nums"
exit 0

