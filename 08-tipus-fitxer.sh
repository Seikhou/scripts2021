#! /bin/bash
#@Seikhou  ASIX-M01
# Febrer 2021
# Programa que ens diu que és cada cosa
#Indica si es o no un directori
#-------------------------------------------------------
#Si el numero d'arguments no és correcte plegar
ERR_NARGS=1
ERR_NOEXISTEIX=1
if [ $# -ne 1 ]
then
        echo "ERROR: num args incorrecte"
        echo "usage: $0 fit"
        exit $ERR_NARGS
fi
#Programa
fit=$1
if [ ! -e $fit ]; then
        echo "Error $fit no existeix"
	exit $ERR_NOEXIST
elif [ -h $fit ] ; then
        echo "$fit és un simbolic link"
elif [ -f $fit ] ; then
        echo "$fit és un regular file"
elif [ -d $fit ] then
        echo "$fit és un directori"
else
        echo "$fit és una altra cosa"
fi
exit 0

                                            
