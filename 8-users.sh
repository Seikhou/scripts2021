#! /bin/bash
# @Seikhou ASIX M01
# Març
# Scripts bàsics
#   mostrar usuaris rebuts per stdin
# -------------------------------------------------------------------
if [ $# -eq 0 ]
then
	echo "ERROR: Ha de tenir com a minim un argument"
	echo "usage: prog noudir[...]"
	exit $ERR_ARG	      
fi

for user in $*
do	
  grep "^$user:" /etc/passwd &> /dev/null
  if [ $? -eq 0 ]; then
    echo $user
  else
    echo $user >> /dev/stderr
  fi	  
done
