#! /bin/bash
# @Seikhou  ASIX-M01
# Abril 2021
# Descripció: Crear l'usuari user1 i observar els valors per defecte que pren el shell, home, uid i gid. Mostrar la 
# línia amb la informació d'usuari del fitxer de comptes d'usuari. S'ha creat algun grup? Mostrar la línia
# d'informació del fitxers de comptes de grup.
#---------------------------------------------------------------------------------------------------------------

useradd user1
cat /etc/passwd | grep ":login:
