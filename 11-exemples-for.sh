******************************************************************************
  M01-ISO Sistemes Operatius
  UF1: Linux Usuari: Instal·lació, configuració i explotació d'un sistema
                     informàtic
******************************************************************************
  A01-04-Redireccionaments,pipes, filtres, variables i shell bàsic
  Exercici 11: pipes, redireccionaments i variables
	- pipes
	- redireccionamnets
	- variables
******************************************************************************

==============================================================================
Ordre: pipes i redireccion
==============================================================================
00) situar-se al directori actiu /tmp/m01 i realizat TOTES les ordres des d'aquest directori (usuari hisx).
	$ cd /tmp/m01


01) llistar el número major i el número menor dels dispositius corresponents a la entrada estàndard, sortida estàndard i d'error. Seguir el lnk fins identificar el device real on esta lligat

ll /dev/std*

02) Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http.

grep ''http'' /etc/services 

03) Desar en un fitxer de nom http.txt tots els serveis que continguin la cadena http però que al mateix temps es mostri per pantalla.

grep ''http'' /etc/services | tee http.txt

04) Desar en un fitxer de nom ftp.txt el llistat de tots els serveis que contenen la cadena ftp ordenats lexicogràficament. La sortida s'ha de mostrar simultàniament per pantalla

grep ''ftp'' /etc/services | sort > ftp

05) Idem exercici anterior però mostrant per pantalla únicament quants serveis hi ha.

grep ''ftp'' /etc/services | sort | tee ftp.txt | wc -l

06) Idem anterior però comptant únicament quants contenen la descrició TLS/SSL

grep ''frp'' /etc/services | sort | tee ftp.txt| grep ''TLS/SSL''| wc -l

07) Llista l'ocupació d'espai del directori tmp fent que els missatges d'error s'ignorin.

du -h /tmp/ 2> /dev/null          

08) Idem anterior desant el resultat a disc.txt i ignorant els errors.

du -h /tmp/ > disc.txt 2> /dev/null

09) Idem enviant tota la sortida (errors i dades) al fitxer disc.txt

du -h /tmp/ &> disc.txt 

10) Afegir al fitxer disc.txt el sumari de l'ocupació de disc dels directoris /boot i /mnt. Els errors cal ignorar-los

du /boot/ /mnt >> disc.txt 2> /dev/null

11) Anomana per a què serveixen les variables:
	HOME, PWD, UID, EUID, HISTFILE; HISTFILESIZE, DISPLAY, SHELL, 
	HOSTNAME, HOSTYPE,LANG, PATH, PPID, PS1, PS2, TERM, USERS

Home- El directori home
Pwd- A on estem
uid- User identification
euid- igual
histfile- 
histfilesize
display
shell
hostname
hostype- Tipus d'arquitectura que utilitzo
lang
path- Ruta
ppid- Parent Pid, número de procés pare 
ps1-
ps2-
term- El tipus de terminal que utilitzem
users- Usuaris



12) Assigna a la variable NOM el teu nom complert (nom i cognoms) i assegura't que s'exporta als subprocessos.

nom=''seikhou konte''

13)Assigna el prompt un format que mostri la hora, la ruta absoluta del directori actiu i el nom d'usuari.

PS1=''\t \w \u''

14) Assigna al prompt el format on mostra el nom d'usuari, de host, el número d'ordre i el número en l'històric d'ordres.

PS1=''\u \h \! \#''

15) Compta quants usuaris hi ha connectats al sistema actualment.


16) Llista quins usuaris hi ha connectats al sistema i quantes connexions tenen establertes. Cal mostrar el login i el numero de connexions per a cada usuari ordenat de major a menor nombre de connexions.





