#! /bin/bash
# @Seikhou ASIX-M01
# Validar que té exàctament 2 args
# i mostar-los
# -------------------------------
# si num args no es correcte plegar
if [ $# -ne 2 ]
then
  echo "ERROR: num args incorrecte"
  echo "usage: prog arg1 arg2"
  exit 1
fi
# xixa
echo "Els dos args son: $1, $2 "
exit 0

