#! /bin/bash
# Seikhou ASIX M01
# Febrer 2021
# Programa que llista directoris i arguments
# ---------------------------------------------------

# Validar nº arguments
ERRARGS=1
ERRDIR=2
if [ $# -ne 1 ]
then
	echo "només 1 argument"
	echo "Usage..."
	exit $ERRARGS
fi

if [ ! -d $1 ]
then
	echo "ha de ser un directori"
	echo "usage..."
	exit $ERRDIR
fi
dir=$1
num=1
llista_elements=$(ls $dir)
for fit in $llista_elements
do
	echo "$num: $fit"
	((num++))
done
exit 0
