#! /bin/bash
# @Seikhou ASIX M01
# Febrer
# Scripts bàsics
#   mostrar l'entrada les línies de +60 chars
# -------------------------------------------------------------------
while read -r line
do	
  num=$(echo "$line" | wc -c)
  if [ "$num" -gt 60 ]; then
    echo $line
  fi	  
