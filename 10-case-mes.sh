#! /bin/bash
# @Seikhou ASIX-M01
# Febrer 2021
# Descripcio: dir els dies que té un més
# Synopsis: prog mes
# ---------------------------------
ERR_NARGS=1
ERR_ARG=2

#1) Validar existeix un arg
if [ $# -ne 1 ]
then
  echo "Error, numero d'arguments no valid"
  echo "Usage: $0 mes"
  exit $ERR_NARGS
fi

#2) Validar si els arguments estan entre els valors: [1-12]
mes=$1
if ! [ $mes -ge 1 -a $mes -le 12 ]
then
  echo "Error, mes no vàlid"
  echo "No és possible, només pot ser: [1-12]"
  echo "Usage: $0 mes"
  exit $ERR_ARG
fi
#3) xixa: determinar numero de dies
case "$mes" in
  "2") 
    dies=28;;
  "4"|"6"|"9"|"11")
    dies=30;;
  *)
    dies=31;;
esac
echo "El mes: $mes, te $dies dies"
exit 0



