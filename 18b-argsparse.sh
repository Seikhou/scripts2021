#! /bin/bash
# @Seikhou ASIX-M01
# Març
# Programa que procesa opcions amb valor i arguments
# -------------------------------

opcions=""
args=""
file=""

while [ -n "$1" ]
do
	case $1 in
		-[bce])
			opcions="$opcions $1";;
		-a)
			file=$2
			shift;;
		-d)
			min=$2
			max=$3
			shift
			shift;;
		*)
			args="$args $1";;

	esac
	shift	
done

echo "Opcions: $opcions"
echo "Arguments: $args"
echo "File: $file"
echo "MIn: $min  Max: $max"
exit 0

