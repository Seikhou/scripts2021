#! /bin/bash
# @Seikhou ASIX-M01
# Abril 2021
# Descripció: Fer una funció que rep un login i mostra per stdout el home de l'u# suari
#--------------------------------------------
# Exercici 21:

function getHome(){
  login=$1
  egrep "$login:" /etc/passwd | cut -d: -f6 2> /dev/null
  if [$? -eq 0];
  then 
	 return 0
  else
	 return 1
  fi
}

# Exercici 22: Funció que rep un o més logins i per cadascun mostra el seu 
# home utilitzant getHome. Validar arguments.

function getHoleList(){
  ERR_NARGS=1  
  status=0  
  #valdidar nº d'args  

  if [ $# -ne 1 ]; 
  then   	
	echo "error nº d'args $# incorrecte"   	
	echo "usage: getHoleList login[...]"
	return $ERR_NARGS
  fi

#Programa
  list_login=$*
  for login in list_login;
  do
	egrep "^$login:" /etc/passwd &> /dev/null   	
	if [ $? -ne 0 ]; 
	then   	  
		echo "error: el login $login no existeix" >> /dev/stderr
		status=2	
	else   	   	  
		getHome $login   	
	fi  
  done   
  return $status
}

# Exercici 23:
# Donat un homedir com a argument mostra per stdout l'ocupació en bytes del# directori:

function getSize(){
  ERR_DIR=1
  homedir=$*  
  if ! [ -d $1];
  then
	echo "Error: directori fisic invàlid"
	echo "Usage: $0 dir"
	return $ERR_DIR
  else
	  return 0
  fi
egrep "^$homedir:[^:]*$"  /etc/passwd 
for dir in homedir:
do 




}




