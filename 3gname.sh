#! /bin/bash
#Seikhou Konte ASIX-M01
#Març 2021
# Programa del gname
#-----------------------------------------
function showuser(){
ERR_NARGS=1
ERR_NOGNAME=2
OK=0
#1) Validar argument
if [ $# -ne 1 ];
then
        echo "Error: nº d'aguments invàlid"
        echo "usage: $0 gname"
        return $ERR_NARGS
fi

#2) Validar si existeix un gname
gname=$1
line=" "
line=$(grep "^$gname:" /etc/group 2> /dev/null)
if [ -z "$line" ];
then
	echo "Error: gname $gname no existeix"
	echo "usage: $0 gname"
	return $ERR_NOGNAME
fi

#3) Mostrar:
gid=$(echo $line | cut -d: -f3)
userList=$(echo $line | cut -d: -f4)
echo "gname: $gname"
echo "gid: $gid"
echo "userList: $userList"
return 0
}
