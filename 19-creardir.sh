#! /bin/bash
# @Seikhou ASIX-M01
# Març
# Crear un nou directori
# -------------------------------

ERR_ARG=1
STATUS=0
#Validar que existeixin 1 o mes arguments

if [ $# -eq 0 ]
then
	echo "ERROR: Ha de tenir com a minim un argument"
	echo "usage: prog noudir[...]"
	exit $ERR_ARG	      
fi

#Per ror directori crear-lo si stdout, no stderr

for dir in $*
do
	mkdir -p $dir &> /dev/null
	if [ $? -eq 0 ]
	then
		echo $dir
	else
		echo "Error en crear: $dir" >> /dev/stderr
		STATUS=2
	fi
done
exit $STATUS
