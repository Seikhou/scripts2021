# !/bin/bash
# @Seikhou  ASIX-M01
# Abril 2021
# Showuserin
# Mostrar un a un les dades dels usuaris rebuts per Stdin
#------------------------------------------------------------------
function showUserIn(){
	status=0
	while read -r login
	do
		line=$(grep "^$login:" /etc/passwd 2> /dev/null)
		if [ $? -ne 0 ];
		then
			echo "Error: login $login inexistent" >> /dev/stderr
			status=$((status+1))
		else
			uid=$(echo $line | cut -d: -f3)       
			gid=$(echo $line | cut -d: -f4)       
			gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)       
			gecos=$(echo $line | cut -d: -f5)       
			home=$(echo $line | cut -d: -f6)       
			shell=$(echo $line | cut -d: -f7)       
			echo "$login $uid $gname($gid) $home $shell"
		fi
	done
	return $status
}
















