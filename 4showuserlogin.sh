#! /bin/bash
# @Seikhou Konte ASIX-M01
# Març 2021
# Programa de mostrar el gname amb show user
# ---------------------------------------------------------------------
function showGROUP(){
ERR_ARGS=1
ERR_NOGNAME=2
OK=2

#1) Validar arguments:
if [$# -ne 1 ];
then
	echo "Error: nº d'arguments invàlid"
	echo "Usage: $0 gname"
	return $ERR_ARGS
fi
#2) Valida si existeix un login:
gname=$1
line=" "
line=$(grep "^$gname:" /etc/passwd 2> /dev/null)

if [-z "$line" ];
then
	echo "Error: gname inexistent"
	echo "Usage : $0 gname"
	return $ERR_NOGNAME
fi

#3) mostrar
 gid=$(echo $line | cut -d: -f3)  
 userList=$(echo $line | cut -d: -f4) 
 echo "gname: $gname" 
 echo "gid: $gid" 
 echo "userList: $userList" 
 return 0
 }  

	
