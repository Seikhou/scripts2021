#! /bin/bash
# @Seikhou ASIX-M01
# Copiar un ficher al directori desti
# -------------------------------

ERR_ARG=1
ERR_FILE=2
ERR_DIR=3

# si num args no es correcte plegar
if [ $# -ne 2 ]
then
	echo "ERROR: num args incorrecte"
	echo "usage: $0"
	exit $ERR_ARG
fi

#validar si es un regular file

if [ ! -f $1 ]
then
	echo "ERROR: el fitxer no existeix"
	echo "usage: $1 un fitxer"
	exit $ERR_FILE
fi

#validar si es un directori

if [ ! -e $2 ] 
then
	echo "ERROR: no es un directori"
	echo "usage: $2 directori desti"
	exit $ERR_DIR
fi

#Copiar $1 al directori $2

file=$1
dir=$2

cp file dir/file
