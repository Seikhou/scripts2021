#! /bin/bash
#@Seikhou ASIX-M01
# Febrer 2021
#Descripció: exemple bucle while
#-------------------------------------------------------------------





#Programa que numera cada linia
while read -r line
do
	echo 
done
exit 0

# Mostrar la entrada estàndard línia a la línia

while read -r line
do
	echo $line
done
exit 0


# mostrar els arguments
num=1
while [ $# -gt 0i ]
do
	echo "$1. $#,  $*"
	num=$((num+1))
	shift
# mostrar els arguments
num=1
while [ -n "$1" ]
do
	echo "$1. $#,  $*"
	num=$((num+1))
	shift
	done
	
exit 0

#Comptador recreixent del arg rebut [n -0]
MIN=0
num=$1
while [ $num -ge $MIN]
do
	echo -n "$num, m"
	(( num--))
done
exit 0

#mostrar del [1-10]
MAX=10
