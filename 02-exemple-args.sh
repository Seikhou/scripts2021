#! /bin/bash
# @Seikhou ASIX M01-ISO
# Febrer 2021
# -------------------------------------------------
echo '$*:' $*

#Llista els elements de l'argument

echo '$@:' $@

#Llista els elements que s'han rebut

echo '$#:' $#

#Numero d'argument/s

echo '$0:' $0

#Nom del programa

echo '$1:' $1

#El primer argument

echo '$10:' ${10} 

#L'argument nº10

echo '$11:' ${11}
#L'argument nº11

nom="puig"
echo "${nom}pelat"

exit 0
#És l'ultim que hem de fer per sortir
