#! /bin/bash
# @Seikhou Konte ASIX-M01
# Març 2021
#Programa de mostrar el Gecos
# -------------------------------------------
function showuser(){
ERR_NARGS=1
ERR_NOLOPGIN=2
OK=2

#1) Validar un argument:
if [ $# -ne 1 ]
then
	echo "Error: nª d'arguments invàlid"
	echo "Usage: $0 gecos"
	return $ERR_NARGS


#2) Validar si existeix un gecos:
gecos=$1
line=" "
line=$(grep ^$gecos:" /etc/passwd 2> /dev/null
