#! /bin/bash
# @Seikhou ASIX-M01
# Copiar un ficher[...] al directori desti
# -------------------------------

ERR_ARG=1
ERR_FILE=2
ERR_DIR=3

# si num args no es correcte plegar
if [ $# -lt 2 ]
then
	echo "ERROR: num args incorrecte"
	echo "usage: $0 file[...] dir"
	exit $ERR_ARG
fi

echo $#
echo $*
echo $* | sed 's/ [^ ]*$//'
echo $* | sed 's/^.* //'
echo

echo $* | cut -d' ' -f1-$(($#-1))
echo $* | cut -d' ' -f$#
