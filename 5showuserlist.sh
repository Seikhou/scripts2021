#! /bin/bash
# @Seikhou Konte ASIX-M01
# Març 2021
# Descripció: Realitza el mateix que la funció showUser però processa n lògins que rep com a argument.
# Validar que el login es valid i que n'hi ha almenys un
# Atenció, feu tot el codi de nou, sense cridar res la funció showuser.
#----------------------------------------------------------------------------------------------------------
function showUserList(){
ERR_ARG=1
stauts=0

# 1) Validar arguments:
if [ $# -ne 1 ];
then
	echo "Error: nº d'argument invàlid"
	echo "Usage: $0 login"
	return $ERR_ARG
fi

# 2) Validar si existeix login:
login_list=$*
for login in $login_list
do
	line=$(grep "^:login:" /etc/passwd 2> /dev/null)
	if [ -z "$line" ];
	then
		echo "Error: login $logn inexistent" >> /dev/stderr
		status=$((status+1))
	else
		 uid=$(echo $line | cut -d: -f3)   
		 gid=$(echo $line | cut -d: -f4)    
		 gname=$(grep "^[^:]*:[^:]*:$gid:" /etc/group | cut -d: -f1)    
		 gecos=$(echo $line | cut -d: -f5)    
		 home=$(echo $line | cut -d: -f6)    
		 shell=$(echo $line | cut -d: -f7)    
		 echo "login: $login"    
		 echo "uid: $uid"   
		 echo "$gname($gid)"   
		 echo "home: $home"   
		 echo "shell: $shell"   
	 fi  
	 done  
	 return $status 
 }


